package com.education.cloud.system.service.api.biz;

import cn.hutool.core.util.ObjectUtil;
import com.education.cloud.system.common.bo.WebsiteNavArticleBO;
import com.education.cloud.system.common.dto.WebsiteNavArticleDTO;
import com.education.cloud.system.service.dao.WebsiteNavArticleDao;
import com.education.cloud.util.base.Result;
import com.education.cloud.util.enums.StatusIdEnum;
import com.education.cloud.util.tools.BeanUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import com.education.cloud.system.service.dao.impl.mapper.entity.WebsiteNavArticle;

/**
 * 站点导航文章
 *
 * @author wuyun
 */
@Component
public class ApiWebsiteNavArticleBiz {

	@Autowired
	private WebsiteNavArticleDao dao;

	public Result<WebsiteNavArticleDTO> get(WebsiteNavArticleBO websiteNavArticleBO) {
		if (StringUtils.isEmpty(websiteNavArticleBO.getNavId())) {
			return Result.error("navId不能为空");
		}
		WebsiteNavArticle websiteNavArticle = dao.getByNavIdAndStatusId(websiteNavArticleBO.getNavId(), StatusIdEnum.YES.getCode());
		if (ObjectUtil.isNull(websiteNavArticle)) {
			return Result.error("没有找到站点导航文章信息");
		}
		return Result.success(BeanUtil.copyProperties(websiteNavArticle, WebsiteNavArticleDTO.class));
	}
}
