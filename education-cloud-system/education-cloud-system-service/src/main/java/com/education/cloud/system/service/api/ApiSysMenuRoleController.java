package com.education.cloud.system.service.api;

import com.education.cloud.util.base.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import com.education.cloud.system.service.api.biz.ApiSysMenuRoleBiz;

/**
 * 菜单角色关联表
 *
 * @author wujing
 */
@RestController
public class ApiSysMenuRoleController extends BaseController {

	@Autowired
	private ApiSysMenuRoleBiz biz;

}
